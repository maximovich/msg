<?php

// change the following paths if necessary
$yii = dirname(__FILE__) . '/../yii/framework/yii.php';
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    error_reporting(E_ALL);
    $config = dirname(__FILE__) . '/protected/config/main.php';
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else {
    $config = dirname(__FILE__) . '/protected/config/main.php';
}

function echoPre($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

require_once($yii);
Yii::createWebApplication($config)->run();
