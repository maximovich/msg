<?php

class User extends CActiveRecord {

    public $birthdayAfterFind;

    const ACTIVE = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, email, password', 'required'),
            array('login', 'unique', 'message' => 'Такой логин уже занят'),
            array('email', 'unique', 'message' => 'Такой email уже занят'),
            array('login', 'length', 'max' => 20, 'min' => 4, 'message' => 'Некорректный логин (длинна от 4 до 20 символов).'),
            array('password', 'length', 'min' => 4, 'message' => 'Некорректный пароль, минимум 4 символа'),
            array('email', 'email'),
            array('login', 'match', 'pattern' => '/^[А-яёЁA-Za-z0-9_\-]+$/u', 'message' => "Некорректный логин можно использовать (А-я, A-z, 0-9, - и _)."),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toMsg' => array(self::HAS_MANY, 'Msg', 'toId'),
            'fromMsg' => array(self::HAS_MANY, 'Msg', 'fromId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'login' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'createdDate' => 'Дата регистрации',
            'lastAccess' => 'Дата последней активности',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->active = self::ACTIVE;
                $this->createdDate = $this->lastAccess = time();
            }
            return true;
        } else
            return false;
    }

    public function getActive() {
        return $this->active;
    }

}
