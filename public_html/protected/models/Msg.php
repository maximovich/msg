<?php

/**
 * This is the model class for table "msg".
 *
 * The followings are the available columns in table 'msg':
 * @property integer $id
 * @property integer $fromId
 * @property integer $toId
 * @property integer $ownerId
 * @property string $text
 * @property string $createdTime
 *
 * The followings are the available model relations:
 * @property User $to
 * @property User $from
 */
class Msg extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'msg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fromId, toId, ownerId, text', 'required'),
            array('fromId, toId, ownerId, status', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, fromId, toId, ownerId, text, createdTime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toUser' => array(self::BELONGS_TO, 'User', 'toId'),
            'fromUser' => array(self::BELONGS_TO, 'User', 'fromId'),
            'ownerUser' => array(self::BELONGS_TO, 'User', 'ownerId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fromId' => 'From',
            'toId' => 'To',
            'ownerId' => 'Owner',
            'text' => 'Text',
            'createdTime' => 'Created Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('fromId', $this->fromId);
        $criteria->compare('toId', $this->toId);
        $criteria->compare('ownerId', $this->ownerId);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('createdTime', $this->createdTime, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Msg the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
