<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel {

    public $login;
    public $password;
    public $rememberMe;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that login and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // login and password are required
            array('login, password', 'required'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'login' => 'Имя пользователя',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->login, $this->password);
            if (!$this->_identity->authenticate())
                return true;
        }
    }

    /**
     * Logs in the user using the given login and password in the model.
     * @return boolean whether login is successful
     */
    public function login($isCheckPassword = true) {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->login, $this->password, $isCheckPassword);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 60*60*24 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } elseif ($this->_identity->errorCode === CBaseUserIdentity::ERROR_PASSWORD_INVALID) {
            $this->addError('password', 'Некорректный логин или пароль.');
        } elseif ($this->_identity->errorCode === UserIdentity::USER_NOTACTIVE) {
            $this->addError('password', 'Вы не можете пользоваться сервисом. Свяжитесь с администрацией сайта.');
        } elseif ($this->_identity->errorCode === UserIdentity::USER_BANNED) {
            $this->addError('password', 'Вы не можете пользоваться сервисом. Вы забанены. Свяжитесь с администрацией сайта.');
        } elseif ($this->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID) {
            $this->addError('password', 'Некорректный логин или пароль.');
        } else {
            return false;
        }
    }

}
