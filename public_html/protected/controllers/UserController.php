<?php

class UserController extends Controller {

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array(
                    'Login',
                ),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array(
                    'Logout',
                    'Index',
                    'List',
                    'Chat',
                    'SendMessage',
                    'UpdateChat',
                    'EditMessage',
                    'EditMessageOpponent',
                    'ViewMsg',
                ),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionLogin() {
        $LoginForm = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'lginForm') {
            echo CActiveForm::validate($LoginForm);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $LoginForm->attributes = $_POST['LoginForm'];
            if ($LoginForm->validate() && $LoginForm->login()) {
                $this->redirect('/user/index/');
            }
        }
        $this->render('login', array('model' => $LoginForm));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionIndex() {
        $this->layout = '//layouts/profile';
        $data = array();
        $this->render('index', $data);
    }

    public function actionList() {
        $this->layout = '//layouts/profile';

        $UserModels = User::model()->findAll('id!=:id', array(':id' => Yii::app()->user->id));

        $data = array(
            'UserModels' => $UserModels,
        );

        $this->render('list', $data);
    }

    public function actionChat() {
        $userId = intval(Yii::app()->request->getQuery('userId'));

        if (!$userId) {
            throw new CHttpException(404, 'Невозможно початиться, пользователь не найден.');
        } elseif ($userId == Yii::app()->user->id) {
            Yii::app()->user->setFlash('error', 'Вы не можете чатиться сам с собой.');
            $this->redirect('/user/list/');
        }

        $this->layout = '//layouts/profile';

        $MsgModel = new Msg();
        $MsgModel->fromId = Yii::app()->user->id;
        $MsgModel->toId = $userId;
        $MsgModel->ownerId = Yii::app()->user->id;

        $MsgModels = Msg::model()->findAll('((fromId=:fromId AND toId=:toId) OR (fromId=:fromId2 AND toId=:toId2)) AND ownerId=:ownerId  ORDER BY id ASC', array(
            ':fromId' => Yii::app()->user->id,
            ':toId' => $userId,
            ':fromId2' => $userId,
            ':toId2' => Yii::app()->user->id,
            ':ownerId' => Yii::app()->user->id,
        ));

        $data = array(
            'opponentId' => $userId,
            'MsgModel' => $MsgModel,
            'MsgModels' => $MsgModels,
        );

        $this->render('chat', $data);
    }

    public function actionSendMessage() {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['Msg'])) {

                $MsgModelFromUser = new Msg();
                $MsgModelToUser = new Msg();

                $MsgModelFromUser->attributes = $_POST['Msg'];
                $MsgModelToUser->attributes = $_POST['Msg'];
                $MsgModelToUser->ownerId = $MsgModelToUser->toId;

                $MsgModelFromUser->save();
                $MsgModelToUser->save();

                echo CJSON::encode(array(
                    'status' => 'success',
                    '_msg' => $this->renderPartial('_msg', array('MsgModel' => $MsgModelFromUser, 'messageEdit' => true), true),
                    'id' => $MsgModelFromUser->id,
                ));
                Yii::app()->end();
            }
        }
    }

    public function actionUpdateChat() {

        if (Yii::app()->request->isAjaxRequest) {
            $lastMessageId = Yii::app()->request->getPost('lastMessageId');
            $opponentId = intval(Yii::app()->request->getPost('opponentId'));

            if ($lastMessageId == 'false') {
                $lastMessageId = 0;
            } else {
                $lastMessageId = intval($lastMessageId);
                if (!$lastMessageId || !$opponentId) {
                    echo CJSON::encode(array(
                        'status' => 'error',
                        'msg' => 'Ошибка получения данных.',
                        'lastMessageId' => $lastMessageId
                    ));
                    Yii::app()->end();
                }
            }

            $MsgModels = Msg::model()->findAll('((fromId=:fromId AND toId=:toId) OR (fromId=:fromId2 AND toId=:toId2)) AND ownerId=:ownerId AND id>:id  ORDER BY id ASC', array(
                ':fromId' => Yii::app()->user->id,
                ':toId' => $opponentId,
                ':fromId2' => $opponentId,
                ':toId2' => Yii::app()->user->id,
                ':ownerId' => Yii::app()->user->id,
                ':id' => $lastMessageId
            ));

            if ($MsgModels) {

                $html = '';

                foreach ($MsgModels as $MsgModelCurrentUser) {
                    $html.=$this->renderPartial('_msg', array('MsgModel' => $MsgModelCurrentUser), true);
                    $lastMessageId = $MsgModelCurrentUser->id;
                }

                echo CJSON::encode(array(
                    'status' => 'success',
                    'msg' => 'Получены нововые сообщения.',
                    'lastMessageId' => $lastMessageId,
                    'html' => $html,
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => 'success',
                    'lastMessageId' => $lastMessageId,
                    'msg' => 'Новых сообщений нет.',
                    'count' => 0,
                ));
            }
        } else {
            echo CJSON::encode(array(
                'status' => 'error',
                'msg' => 'Не аякс запрос.'
            ));
        }
    }

    public function actionEditMessage() {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_POST['Msg'])) {

                $id = intval($_POST['Msg']['id']);

                if (!$id) {
                    echo CJSON::encode(array(
                        'status' => 'error',
                        'msg' => 'Неверный идентификатор сообщения',
                    ));
                    Yii::app()->end();
                }

                $MsgModel = Msg::model()->findByPk($id);
                $MsgModel2 = Msg::model()->find('text=:text AND ownerId!=:ownerId', array(
                    ':text' => $MsgModel->text,
                    ':ownerId' => Yii::app()->user->id
                ));

                $MsgModel->text = $_POST['Msg']['text'];
                $MsgModel2->text = $_POST['Msg']['text'];

                if ($MsgModel->save() && $MsgModel2->save()) {
                    $MsgModel2->status = 2;
                    $MsgModel2->save();
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'text' => $MsgModel->text,
                        'MsgModel2' => $MsgModel2->text,
                    ));
                } else {
                    echo CJSON::encode(array(
                        'status' => 'error',
                        'msg' => 'Сообщение не сохранено.',
                    ));
                }
                Yii::app()->end();
            }
        }
    }

    public function actionEditMessageOpponent() {
        if (Yii::app()->request->isAjaxRequest) {

            $MsgModel = Msg::model()->find('status=:status AND ownerId=:ownerId', array(
                ':status' => 2,
                ':ownerId' => Yii::app()->user->id
            ));

            if ($MsgModel) {
                echo CJSON::encode(array(
                    'status' => 'success',
                    'text' => $MsgModel->text,
                    'id' => $MsgModel->id,
                ));
                $MsgModel->status = 0;
                $MsgModel->save();
            } else {
                echo CJSON::encode(array(
                    'status' => 'success',
                    'text' => '',
                    'id' => '',
                ));
            }
            Yii::app()->end();
        }
    }

    /**
     * Это действие для отладки
     * вида сообщения.
     */
    public function actionViewMsg() {
        $MsgModel = Msg::model()->find('ownerId=:ownerId', array(':ownerId' => Yii::app()->user->id));
        $this->render('_msg', array('MsgModel' => $MsgModel));
    }

}
