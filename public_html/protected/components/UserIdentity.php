<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    private $isGetPassword;

    const USER_NOTACTIVE = 3;
    const USER_BANNED = -1;
    const SALT = '(&^*(#&^)(@&$_)@*%_*ONkjdbfpusiпавпа◊㎡ ●҉★☜°◊㎡ ●҉★☜°◊㎡ ҉★☜°◊°◊°◊°◊в';

    public function __construct($login, $password, $isGetPassword = true) {
        parent::__construct($login, $password);
        $this->isGetPassword = $isGetPassword;
    }

    public function authenticate() {
        $record = User::model()->findByAttributes(array('login' => $this->username));
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            if ($record->password !== hash('sha256', hash('sha256', $this->password . self::SALT) . hash('sha256', self::SALT))) {
                if ($this->isGetPassword) {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                } else {
                    $this->_id = $record->id;
                    $this->setState('login', $record->login);
                    $this->errorCode = self::ERROR_NONE;
                }
            } else {
                if (!$record->active) {
                    $this->errorCode = self::USER_NOTACTIVE;
                } elseif ($record->active == 1) {
                    $this->errorCode = self::USER_BANNED;
                } else {
                    $this->_id = $record->id;
                    $this->setState('login', $record->login);
                    $this->errorCode = self::ERROR_NONE;
                }
            }
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
