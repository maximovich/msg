<?php

class Controller extends CController {

    public $layout = '//layouts/column1';
    public $menu = array();
    public $breadcrumbs = array();
    public $pageKw = 'Пирамида';
    public $pageDesc = 'Пирамида';
    public $pageRobotsIndex = true;

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function display_seo() {
        echo "\t" . '' . PHP_EOL;
        echo "\t" . '<meta name="description" content="', CHtml::encode($this->pageDesc), '">' . PHP_EOL;
        echo "\t" . '<meta name="keywords" content="', CHtml::encode($this->pageKw), '">' . PHP_EOL;

        if ($this->pageRobotsIndex == false) {
            echo '<meta name="robots" content="noindex">' . PHP_EOL;
        }
    }

}
