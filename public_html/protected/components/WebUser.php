<?php

class WebUser extends CWebUser {

    protected function afterLogin($fromCookie) {
        $user = User::model()->findByPk(Yii::app()->user->id);
        $user->lastAccess = time();
        $user->save();
        parent::afterlogin($fromCookie);
    }

}
