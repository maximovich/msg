<?php

class CaptchaAction extends CCaptchaAction {

    public function validate($input, $caseSensitive) {
        $code = $this->getVerifyCode();
        $valid = $caseSensitive ? ($input === $code) : strcasecmp($input, $code) === 0;
        if(Yii::app()->request->isAjaxRequest) return $valid;
        $session = Yii::app()->session;
        $session->open();
        $name = $this->getSessionKey() . 'count';
        $session[$name] = $session[$name] + 1;
        if ($session[$name] > $this->testLimit && $this->testLimit > 0)
            $this->getVerifyCode(true);
        return $valid;
    }

}
