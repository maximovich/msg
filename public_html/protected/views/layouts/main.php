<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php $this->display_seo(); ?>
    </head>

    <body>

        <div class="topPreloder">
            <div class="innerTopPreloder">
                <?php
                $this->widget('bootstrap.widgets.TbProgress', array(
                    'percent' => 100,
                    'striped' => true,
                    'animated' => true,
                ));
                ?>
            </div>
        </div>
        <div class="overlay"></div>

        <div class="container" id="page" style="disabled: disabled;">

            <?php /* <div id="header">
              <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
              </div><!-- header --> */ ?>

            <div id="mainmenu">
                <?php
                $enterArray = array();

                if (Yii::app()->user->isGuest) {
                    $enterArray = array(
                        'label' => 'Вход',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Вход', 'url' => '/user/login/'),
                        ),
                    );
                } else {
                    $enterArray = array(
                        'label' => '<i class="icon icon-user"></i> ' . Yii::app()->user->login,
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Кабинет', 'url' => '/user/index/'),
                            '---',
                            array('label' => 'Выход', 'url' => '/user/logout/'),
                        )
                    );
                }

                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'type' => 'null', // null or 'inverse'
                    'brand' => Yii::app()->name,
                    'brandUrl' => '/',
                    'collapse' => true,
                    'fixed' => false,
                    'items' => array(
                        array('class' => 'bootstrap.widgets.TbMenu',
                            'items' => array(
                                array('label' => 'Главная', 'url' => array('/site/index/'))
                            )
                        ),
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'encodeLabel' => false,
                            'htmlOptions' => array('class' => 'pull-right'),
                            'items' => array(
                                '---',
                                $enterArray
                            ),
                        )
                )));
                ?>
            </div><!-- mainmenu -->

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>
            <br />
            <?php
            $this->widget('bootstrap.widgets.TbAlert', array(
                'block' => true,
                'fade' => true,
                'closeText' => '&times;', // false equals no close link
                'userComponentId' => 'user',
                'alerts' => array(
                    'error',
                    'success',
                    'info',
                    'warning',
                ),
            ));
            ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <hr />
                Copyright &copy; <?php echo date('Y'); ?> by <?php echo Yii::app()->name; ?><br/>
                Красноярск
            </div><!-- footer -->

        </div><!-- page -->
    </body>
</html>
