<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
    <div class="row-fluid">
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbMenu', array(
                'type' => 'list',
                'items' => array(
                    array(
                        'label' => 'Кабинет',
                        'url' => '/user/index/',
                    ),
                    array(
                        'label' => 'Пользователи',
                        'url' => '/user/list/',
                    ),
                    '',
                    
            )));
            ?>
        </div>
        <div class="span9">
            <?php echo $content; ?>
        </div>

    </div>
</div>
<?php $this->endContent(); ?>