<?php foreach ($UserModels as $UserModel): ?>
    <div class="well well-small">
        <div class="row-fluid">
            <div class="span3">
                <?php echo $UserModel->login; ?>
            </div>
            <div class="span3">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => 'Початиться',
                    'url' => '/user/chat/?userId='.$UserModel->id,
                    'type' => 'success',
                    'htmlOptions' => array('class' => ''))
                );
                ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>