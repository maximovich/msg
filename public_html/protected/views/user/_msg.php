<div class="row-fluid p10 <?php echo $MsgModel->fromId == Yii::app()->user->id ? 'backgroundOther' : ''; ?>">
    <div style="padding: 10px;">
        <div class="span3 br1">
            <div><?php echo $MsgModel->fromUser->login; ?></div>
            <div><?php echo date('d.m.Y H:i:s', $MsgModel->createdTime?strtotime($MsgModel->createdTime):time()); ?></div>
        </div>
        <div class="span8">
            <div id="editableMessage<?php echo $MsgModel->id; ?>">
                <?php echo $MsgModel->text; ?>
            </div>
        </div>
        <div class="span1 editableMessage">
            <?php
            if(isset($messageEdit) && $messageEdit) {
                $this->widget('bootstrap.widgets.TbButton', array(
                    'type' => 'primary',
                    'id' => $MsgModel->id,
                    'icon' => 'pencil',
                    'label' => '',
                    'htmlOptions' => array(
                        'class' => 'editMessage'
                    ),
                ));
            }
            ?>
        </div>
    </div>
</div>