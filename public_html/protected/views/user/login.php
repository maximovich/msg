<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Вход';
$this->breadcrumbs = array(
    'Вход',
);
?>

<h2>Вход</h2>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'lginForm',
    'enableClientValidation' => true,
    'htmlOptions' => array('class' => ''),)
);

echo $form->textFieldRow($model, 'login', array('class' => 'span3'));
echo $form->passwordFieldRow($model, 'password', array('class' => 'span3'));
echo $form->checkboxRow($model, 'rememberMe');
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => 'Войти'
));
?>
<?php
$this->endWidget();
unset($form);
?>
