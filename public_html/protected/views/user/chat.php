<div class="row-fluid">
    <div class="chat" id="chatContent">
        <?php if ($MsgModels): ?>
            <?php $empty = false; ?>
            <?php foreach ($MsgModels as $MsgModelCurrentUser): ?>
                <?php $this->renderPartial('_msg', array('MsgModel' => $MsgModelCurrentUser)); ?>
                <?php $lastMessageId = $MsgModelCurrentUser->id; ?>
            <?php endforeach; ?>
        <?php else : ?>
            <?php $lastMessageId = false; ?>

        <?php endif; ?>
    </div>
    <div id="editableMesageBlock">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'msgEdit',
            'htmlOptions' => array('class' => '')));
        ?>
        <div class="span4">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'id' => 'saveMessage',
                'type' => 'success',
                'label' => 'сохранить',
                'htmlOptions' => array(
                    'style' => 'width: 100%; height: 50px;'
                ),
            ));
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'id' => 'cancelMessage',
                'type' => 'danger',
                'label' => 'Отменить',
                'htmlOptions' => array(
                    'style' => 'width: 100%; height: 50px;'
                ),
            ));
            ?>
        </div>
        <div class="span8">
            <?php
            echo $form->textArea($MsgModel, 'text', array('class' => 'span12', 'id' => 'textMessageEdit', 'style' => 'height: 100px; width: 100%;'));
            echo $form->hiddenField($MsgModel, 'fromId');
            echo $form->hiddenField($MsgModel, 'toId');
            echo $form->hiddenField($MsgModel, 'ownerId');
            echo $form->hiddenField($MsgModel, 'id', array('id' => 'editMessageId'));
            ?>
        </div>
        <?php
        $this->endWidget();
        unset($form);
        ?>
    </div>
</div>
<br />
<div class="row-fluid">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'msg',
        'htmlOptions' => array('class' => '')));
    ?>
    <div class="span4">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'id' => 'sendMessage',
            'type' => 'primary',
            'label' => 'Отправить',
            'htmlOptions' => array(
                'style' => 'width: 100%; height: 50px;'
            ),
        ));
        ?>
    </div>
    <div class="span8">
        <?php
        echo $form->textArea($MsgModel, 'text', array('class' => 'span12', 'id' => 'textMessage', 'style' => 'height: 50px; width: 100%;'));
        echo $form->hiddenField($MsgModel, 'fromId');
        echo $form->hiddenField($MsgModel, 'toId');
        echo $form->hiddenField($MsgModel, 'ownerId');
        ?>
    </div>
    <?php
    $this->endWidget();
    unset($form);
    ?>
</div>


<script src="/js/jquery.scrollTo.min.js"></script>
<script>
    var cheackUpdate = true;
    var lastMessageId = <?php echo $lastMessageId ? $lastMessageId : 'false'; ?>;
    var opponentId = <?php echo $opponentId; ?>;
    var lastMyMessageId = false;
    var timeEditableMessage = 60;
    var editTimeMessage = 0;
    var editMessageId = 0;

    $().ready(function() {
        $('#chatContent').scrollTo('max');

        $('#sendMessage').click(function(e) {
            e.preventDefault();
            if (!$('#textMessage').val()) {
                return;
            }
            $.ajax({
                success: function(data) {
                    if (data.status == "success") {
                        $('.editableMessage').html('');
                        $('#textMessage').val('');
                        $('#chatContent').append(data._msg);
                        editTimeMessage = timeEditableMessage;
                        lastMessageId = data.id;
                        lastMyMessageId = data.id;
                        $('#chatContent').scrollTo('max', 1000);
                        $('#textMessage').val('');
                    } else {
                        console.log(data);
                    }
                },
                type: 'post',
                url: '/user/sendMessage/',
                data: $('#msg').serialize(),
                cache: false,
                dataType: 'json'
            });
        });

        function updateChat() {
            if (!cheackUpdate) {
                return;
            }
            cheackUpdate = false;
            $.ajax({
                success: function(data) {
                    if (data.status == "success") {
                        $('#chatContent').append(data.html);
                        if (data.lastMessageId != lastMessageId) {
                            $('#chatContent').scrollTo('max', 1000);
                            lastMessageId = data.lastMessageId;
                        }
                        cheackUpdate = true;
                    } else {
                        console.log(data);
                        cheackUpdate = true;
                    }
                },
                fail: function(data) {
                    cheackUpdate = true;
                },
                type: 'post',
                url: '/user/updateChat/',
                data: {lastMessageId: lastMessageId, opponentId: opponentId},
                cache: false,
                dataType: 'json'
            });
        }
        setInterval(updateChat, 2000);

        $('.editMessage').live('click', function(e) {
            var btnEdit = $(e.target);
            var id = btnEdit.attr('id');
            if (!id) {
                btnAdd = btnEdit.parent();
                id = btnAdd.attr('id');
            }
            editMessageId = id;
            var text = $('#editableMessage' + id).text();

            $('#textMessageEdit').val($.trim(text));
            $('#editableMesageBlock').slideToggle();

        });

        $('#saveMessage').click(function(e) {
            e.preventDefault();
            if (!$('#textMessageEdit').val()) {
                return;
            }

            $('#editMessageId').val(editMessageId);

            $.ajax({
                success: function(data) {
                    if (data.status == "success") {
                        $('#editableMessage' + editMessageId).html(data.text);
                        $('#editableMesageBlock').slideToggle();
                        $('#textMessageEdit').val('');
                    } else {
                        console.log(data);
                    }
                },
                type: 'post',
                url: '/user/editMessage/',
                data: $('#msgEdit').serialize(),
                cache: false,
                dataType: 'json'
            });
        });

        $('#cancelMessage').click(function(e) {
            e.preventDefault();
            $('#editableMesageBlock').slideToggle();
        });

        function checkEditMessage() {
            if (editTimeMessage) {

                editTimeMessage -= 1;

                if (editTimeMessage <= 0) {
                    $('.editableMessage').html('');
                    return;
                }

            }
        }
        setInterval(checkEditMessage, 1000);

        function checkEditMessageOpponent() {
            $.ajax({
                success: function(data) {
                    if (data.status == "success") {
                        if (data.id) {
                            $('#editableMessage' + data.id).html(data.text);
                        }
                    } else {
                        console.log(data);
                    }
                },
                type: 'post',
                url: '/user/editMessageOpponent/',
                data: $('#msgEdit').serialize(),
                cache: false,
                dataType: 'json'
            });
        }
        setInterval(checkEditMessageOpponent, 2000);

    });
</script>